
#include "../TrigLArNoiseBurstHypoTool.h"
#include "../TrigLArNoiseBurstAlg.h"
#include "../TrigL2CaloLayersHypoTool.h"
#include "../TrigL2CaloLayersAlg.h"

DECLARE_COMPONENT( TrigLArNoiseBurstHypoTool )
DECLARE_COMPONENT( TrigLArNoiseBurstAlg )
DECLARE_COMPONENT( TrigL2CaloLayersHypoTool )
DECLARE_COMPONENT( TrigL2CaloLayersAlg )

