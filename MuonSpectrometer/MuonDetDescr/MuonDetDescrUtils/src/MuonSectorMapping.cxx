/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/no_bitwise_op_warning.h"
#include "MuonDetDescrUtils/MuonSectorMapping.h"

namespace Muon {
    bool MuonSectorMapping::s_debug = false;
}
